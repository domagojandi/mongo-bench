var crc32 = require('fast-crc32c');
var EventEmitter = require('events').EventEmitter;

var dbWrapper = function(db, options) {
    if (db.__mongoBench) {
        return db;  // do not allow multiple dbWrapper layering!
    }
    options = options || {};

    var dbEmitter = null;
    var colEmitters = {}; // emitters for collections
    var logsCollection = options.logsCollection ? db.collection(options.logsCollection) : null;
    var calculateQueryFingerprint = options.calculateQueryFingerprint == null ? true : options.calculateQueryFingerprint;
    var dbName = db.databaseName;

    var getQueryFingerprint = function(data, initial) {
        if (!initial) {
            initial = '';
        }
        var local = '';
        Object.keys(data || {}).sort().forEach(function(k) {
            var v = data[k];
            if (v === undefined) return;
            local += k;
            if (v && typeof v === 'object' && !Array.isArray(v)) {
                local += getQueryFingerprint(v, initial);
            }
        });
        initial += local;
        return initial;
    };

    var logResponseTime = function(action, collection, query, duration) {
        var fingerprint = 0;
        var queryEventData = {
            'action': action,
            'duration': duration,
            'timestamp': new Date(),
            'database': dbName,
            'collection': collection,
            'query': JSON.stringify(query)
        };
        if (calculateQueryFingerprint) {
            if (Array.isArray(query) && (action === 'insertMany' || action === 'insert')) {
                fingerprint = getQueryFingerprint(query[0]);
            } else {
                fingerprint = getQueryFingerprint(query);
            }
            queryEventData.queryFingerprint = crc32.calculate(fingerprint).toString();
        }
        if (logsCollection) {
            // if user wants to write its data to mongo, write it
            logsCollection.insertOne(queryEventData);
        }
        delete queryEventData._id;
        if (dbEmitter) {
            dbEmitter.emit('query', queryEventData);
        }
        if (colEmitters[collection]) {
            colEmitters[collection].emit('query', queryEventData);
        }
    };

    var queryMongo = function(col, collName, action, args, query, isCursor) {
        var callback = args[args.length-1];
        query = query || args[0];
        var now = Date.now();
        var logAction = '';
        if (isCursor) {
            logAction = 'find.' + action;
        } else {
            logAction = action;
        }
        if (typeof callback === 'function') {
            args[args.length-1] = function() {
                if (arguments[0]) {
                    // if error
                    return callback.apply(null, arguments);
                } else {
                    var timeDiff = Date.now() - now;
                    logResponseTime(logAction, collName, query, timeDiff);
                    callback.apply(null, arguments);
                }
            };
            return col[action].apply(col, args);
        } else {
            return col[action].apply(col, args).then(function(r) {
                var timeDiff = Date.now() - now;
                logResponseTime(logAction, collName, query, timeDiff);
                return r;
            });
        }
    };

    var mongoCursor = function(col, collName, query, fields, options) {
        var cursor = col.find(query, fields, options);
        var duration = 0;
        var now = Date.now();

        var cursorCustomMethods = {
            'count': function() {
                return queryMongo(cursor, collName, 'count', arguments, query, true);
            },
            'each': function(iterator) {
                cursor.forEach(function(err, item) {
                    if (!err && item == null) {
                        var timeDiff = Date.now() - now;
                        logResponseTime('find.each-deprecated', collName, query, timeDiff);
                    }
                    return iterator(err, item);
                });
            },
            'forEach': function(iterator, callback) {
                var now = Date.now();
                return cursor.forEach(iterator, function(err) {
                    if (!err) {
                        var timeDiff = Date.now() - now;
                        logResponseTime('find.forEach', collName, query, timeDiff);
                    }
                    return callback(err);
                });
            },
            'next': function(callback) {
                var now = Date.now();
                return cursor.next().then(res => {
                    duration += Date.now() - now;
                    if (res == null) {
                        logResponseTime('find.next-total', collName, query, duration);  // iteration over
                    }
                    if (callback) {
                        return callback(null, res);
                    } else {
                        return res;
                    }
                }).catch(err => {
                    if (callback) {
                        return callback(err);
                    } else {
                        return Promise.reject(err);
                    }
                });
            },
            'toArray': function() {
                return queryMongo(cursor, collName, 'toArray', arguments, query, true);
            }
        };

        return Object.assign(Object.create(cursor), cursorCustomMethods);
    };

    var collectionWrapper = function(db, collName, options) {
        var col = db.collection(collName, options);

        var collectionCustomMethods = {
            'find': function(query,fields,options) {
                return mongoCursor(col, collName, query, fields, options);
            },
            'count': function() {
                return queryMongo(col, collName, 'count', arguments);
            },
            'findOne': function() {
                return queryMongo(col, collName, 'findOne', arguments);
            },
            'findOneAndReplace': function() {
                return queryMongo(col, collName, 'findOneAndReplace', arguments);
            },
            'findOneAndUpdate': function() {
                return queryMongo(col, collName, 'findOneAndUpdate', arguments);
            },
            'findOneAndDelete': function() {
                return queryMongo(col, collName, 'findOneAndDelete', arguments);
            },
            'findAndRemove': function() {
                return queryMongo(col, collName, 'findAndRemove', arguments);
            },
            'insert': function() {
                return queryMongo(col, collName, 'insert', arguments);
            },
            'insertMany': function() {
                return queryMongo(col, collName, 'insertMany', arguments);
            },
            'insertOne': function() {
                return queryMongo(col, collName, 'insertOne', arguments);
            },
            'update': function() {
                return queryMongo(col, collName, 'update', arguments);
            },
            'updateMany': function() {
                return queryMongo(col, collName, 'updateMany', arguments);
            },
            'updateOne': function() {
                return queryMongo(col, collName, 'updateOne', arguments);
            },
            'deleteMany': function() {
                return queryMongo(col, collName, 'deleteMany', arguments);
            },
            'deleteOne': function() {
                return queryMongo(col, collName, 'deleteOne', arguments);
            },
            'remove': function() {
                return queryMongo(col, collName, 'remove', arguments);
            },
            'skipNextLog': function() {
                return col;
            },
            'onQuery': function(callback) {
                colEmitters[collName] = colEmitters[collName] || new EventEmitter();
                colEmitters[collName].on('query', callback);
            }
        };

        return Object.assign(Object.create(col), collectionCustomMethods);
    };

    return Object.assign(Object.create(db), {
        'collection': function(name, options, callback) {
            if (typeof options === 'function') {
                callback = options;
                options = null;
            }
            var collection = db.__mongoBench ? db.collection(name, options) : collectionWrapper(db, name, options);
            if (typeof callback === 'function') {
                return callback(null, collection);
            } else {
                return collection;
            }
        },
        'onQuery': function(callback) {
            dbEmitter = dbEmitter || new EventEmitter();
            dbEmitter.on('query', callback);
        },
        'setMongoBenchOptions': function(options) {
            this.__mongoBench.options = options;
            logsCollection = options.logsCollection ? db.collection(options.logsCollection) : null;
            calculateQueryFingerprint = options.calculateQueryFingerprint == null ? true : options.calculateQueryFingerprint;
        },
        '__mongoBench': {
            'db': db,
            'options': options
        },
        'getOriginalInstance': function() {
            var db = this.__mongoBench.db;
            return typeof db.getOriginalInstance === 'function' ? db.getOriginalInstance() : db;
        }
    });
};

var mongoDbWrapper = function(mongo, options) {
    var oldDb = mongo.Db.__mongoBench ? mongo.Db.__mongoBench.db : mongo.Db;
    var newDb = function() {
        return dbWrapper(oldDb.apply(oldDb, arguments), options);
    };
    newDb.__mongoBench = {
        'db': oldDb
    };
    mongo.Db = newDb;
};

module.exports = {
    'mongo': mongoDbWrapper,
    'db': dbWrapper
};

