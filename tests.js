// SIMPLE TESTS to ensure mongo commands are working after wrapping them
// TODO: more complex tests which will actually check if manipualating with data works ok
var mongo = require('mongodb'); 
var async = require('async');
var dbName = 'mirkotest';
var collectionName = 'test';
var benchmarkCollectionName = 'bench';

var db = new mongo.Db(dbName, new mongo.Server('10.42.1.31', 27017, {auto_reconnect: true, poolSize: 1}), {native_parser: false, safe: true}); 
db.open(); 
var dbb = require('./index')(db, {
	'logsCollection': benchmarkCollectionName
	//'calculateQueryFingerprint': null  // enabled by default
});
var coll = dbb.collection(collectionName);

dbb.onQuery(function(data) {
	//console.log("QUERY DATA on DB level", data);
});
coll.onQuery(function(data) {
	//console.log("QUERY DATA on COLLECTION level", data);
});

function doCallbackTests(callback) {
	async.waterfall([
		function(next) {
			console.log("cleaning up the database");
			db.collection(collectionName).deleteMany({}, err => next(err));
		},
		function(next) {
			coll.insertOne({ a: 1 }, err => next(err));
		},
		function(next) {
			console.log("added one doc using insertOne");
			coll.insertMany([{ a: 2 }, { a: 3 }], err => next(err));
		},
		function(next) {
			console.log("added two docs using insertMany");
			coll.insert([{ a: 4 }, { z: 100 }, { z: 101 }, { z: 102 }], err => next(err));
		},
		function(next) {
			console.log("added 3 docs using insert");
			coll.update({ a: 1 }, { $set: { b: 1 }}, err => next(err));
		},
		function(next) {
			console.log("updating one doc using update");
			coll.updateOne({ a: 2 }, { $set: { b: 2 }}, err => next(err));
		},
		function(next) {
			console.log("updating one doc using updateOne");
			coll.updateMany({ a: { $gt: 2 } }, { $set: { b: 3 }}, err => next(err));
		},
		function(next) {
			console.log("updating many documents using updateMany");
			coll.count({}, (err, cnt) => next(err, cnt));
		},
		function(count, next) {
			console.log("Found total " + count + " records");
			coll.findOne({b: 3}, (err, doc) => next(err, doc));
		},
		function(doc, next) {
			console.log("Found document b: 3 => ", doc);
			coll.findAndRemove({a: 4}, (err, doc) => next(err, doc));
		},
		function(doc, next) {
			console.log("Found and removed doc:", doc);
			coll.findOneAndDelete({a : 3}, (err, doc) => next(err, doc));
		},
		function(doc, next) {
			console.log("foundOneAndDelete:", doc);
			coll.findOneAndUpdate({ a: 2, b: 2 }, { $set: { c: 3 } }, (err, doc) => next(err, doc));
		},
		function(doc, next) {
			console.log("findOneAndUpdate:", doc);
			coll.findOneAndReplace({ a: 1, b: 1 }, { a: -1, b: -1, c: -1 }, (err, doc) => next(err, doc));
		},
		function(doc, next) {
			console.log("findOneAndReplace:", doc);
			coll.find({}).count((err, cnt) => next(err, cnt));
		},
		function(count, next) {
			console.log("count of all remaining docs. should be 2: " + count);
			coll.find({ a: -1 }).toArray((err, docs) => next(err, docs));
		},
		function(docs, next) {
			console.log("doc with -1: ", docs);
			var c = coll.find({});
			c.next((err1, doc) => {
				console.log("first document from cursor using next:", doc);
				c.next((err2, doc) => {
					console.log("2nd doc from cursor:", doc);
					next(err1 || err2);
				});
			});
		},
		function(next) {
			console.log("logging out all the documents with b:2");
			coll.find({ b: 2 }).forEach(doc => {
				console.log("doc:", doc);
			}, err => {
				console.log("forEach completed");
				next(err);
			});
		},
		function(next) {
			coll.remove({ z: 100 }, err => next(err));
		},
		function(next) {
			console.log("removed one document using remove");
			coll.removeOne({ z: 101 }, err => next(err));
		},
		function(next) {
			console.log("removed one document using removeOne");
			coll.removeMany({ a: {$gt: -1}}, err => next(err));
		},
		function(next) {
			console.log("removed all the document with a > -1 using removeMany");
			console.log("There should only remain two docs");
			next();
		}
	], function(err) {
		callback(err);
	});
}

function doPromiseTests(callback) {
	async.waterfall([
		function(next) {
			console.log("cleaning up the database");
			db.collection(collectionName).deleteMany({}).then(() => next());
		},
		function(next) {
			console.log("database cleaned up");
			coll.insertOne({ a: 1 }).then(() => next());
		},
		function(next) {
			console.log("added one doc using insertOne");
			coll.insertMany([{ a: 2 }, { a: 3 }]).then(() => next());
		},
		function(next) {
			console.log("added two docs using insertMany");
			coll.insert([{ a: 4 }, { z: 100 }, { z: 101 }, { z: 102 }]).then(() => next());
		},
		function(next) {
			console.log("added 3 docs using insert");
			coll.update({ a: 1 }, { $set: { b: 1 }}).then(() => next());
		},
		function(next) {
			console.log("updating one doc using update");
			coll.updateOne({ a: 2 }, { $set: { b: 2 }}).then(() => next());
		},
		function(next) {
			console.log("updating one doc using updateOne");
			coll.updateMany({ a: { $gt: 2 } }, { $set: { b: 3 }}).then(() => next());
		},
		function(next) {
			console.log("updating many documents using updateMany");
			coll.count({}).then(cnt => next(null, cnt));
		},
		function(count, next) {
			console.log("Found total " + count + " records");
			coll.findOne({b: 3}).then(doc => next(null, doc));
		},
		function(doc, next) {
			console.log("Found document b: 3 => ", doc);
			coll.findAndRemove({a: 4}).then(doc => next(null, doc));
		},
		function(doc, next) {
			console.log("Found and removed doc:", doc);
			coll.findOneAndDelete({a : 3}).then(doc => next(null, doc));
		},
		function(doc, next) {
			console.log("foundOneAndDelete:", doc);
			coll.findOneAndUpdate({ a: 2, b: 2 }, { $set: { c: 3 } }).then(doc => next(null, doc));
		},
		function(doc, next) {
			console.log("findOneAndUpdate:", doc);
			coll.findOneAndReplace({ a: 1, b: 1 }, { a: -1, b: -1, c: -1 }).then(doc => next(null, doc));
		},
		function(doc, next) {
			console.log("findOneAndReplace:", doc);
			coll.find({}).count().then(cnt => next(null, cnt));
		},
		function(count, next) {
			console.log("count of all remaining docs. should be 2: " + count);
			coll.find({ a: -1 }).toArray().then(docs => next(null, docs));
		},
		function(docs, next) {
			console.log("doc with -1: ", docs);
			var c = coll.find({});
			c.next().then(doc => {
				console.log("first document from cursor using next:", doc);
				c.next().then(doc => {
					console.log("2nd doc from cursor:", doc);
					next();
				});
			});
		},
		function(next) {
			console.log("logging out all the documents with b:2");
			coll.find({ b: 2 }).forEach(doc => {
				console.log("doc:", doc);
			}, err => {
				console.log("forEach completed");
				next(err);
			});
		},
		function(next) {
			coll.remove({ z: 100 }).then(() => next());
		},
		function(next) {
			console.log("removed one document using remove");
			coll.removeOne({ z: 101 }).then(() => next());
		},
		function(next) {
			console.log("removed one document using removeOne");
			coll.removeMany({ a: {$gt: -1}}).then(() => next());
		},
		function(next) {
			console.log("removed all the document with a > -1 using removeMany");
			console.log("There should only remain two docs");
			next();
		}
	], function(err) {
		callback(err);
	});
}

setTimeout(() => {
	doCallbackTests(function(err) {
		if (err) {
			console.log("some callback test failed", err);
			process.exit();
		} else {
			console.log("all callback tests completed successfully");
		}
		doPromiseTests(function(err) {
			if (err) {
				console.log("some promise test failed", err);
				process.exit();
			} else {
				console.log("all promise tests completed successfully");
				console.log("ALL COMPLETED WITHOUT ERROR");
				process.exit();
			}
		});
	});
}, 1000);